#
# GDB Init script for the Coldfire 5441x processor.
#
# The main purpose of this script is to configure the
# DRAM controller so code can be loaded.
#

define hook-quit
	set confirm off
end

# multilink
target remote localhost:3333
# bdm (old)
# target remote | m68k-bdm-gdbserver pipe /dev/tblcf0


# load file now only, once regs and rambar are initialized

load bin/cf64k.elf
file bin/cf64k.elf

set $pc=_start

set print pretty
set print asm-demangle
display/i $pc
select-frame 0

continue
