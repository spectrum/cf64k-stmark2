/*
 * serial.c
 *
 * mcf54416 sram bootloader serial driver
 *
 * Copyright 2016 Angelo Dureghello - Sysam <angelo@sysam.it>
 *
 * This file is part of cf64k firmware application.
 *
 * cf64k is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cf64k.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Note: most of the code and related defines comes from
 *       the U-boot bootloader (www.denx.de).
 */

#include "globals.h"
#include "serial.h"
#include "config.h"
#include "gpio.h"
#include "power.h"

#define GETCHAR_TIMEOUT_TICKS_MAX	0x200000

extern gd_t gd;

void setup_mux_clock_uart(int port)
{
	struct gpio_regs *gpio = (struct gpio_regs *)MMAP_GPIO;
	struct pm_regs *pm = (struct pm_regs *)MMAP_PM;

	/* UART0 clock enable and pinmux setup */
	out_8(&pm->pmcr0, 24);
	setbits_8(&gpio->par_uart0, 0x0f);
}

void init_serial_port(u8 port_idx)
{
	struct uart_regs *uart = (struct uart_regs *)CONFIG_SYS_UART_BASE;
        u32 counter;
	u32 baudrate;

        setup_mux_clock_uart(port_idx);

	baudrate = CONFIG_BAUDRATE;

	/* write to SICR: SIM2 = uart mode,dcd does not affect rx */
	writeb(UART_UCR_RESET_RX, &uart->ucr);
	writeb(UART_UCR_RESET_TX, &uart->ucr);
	writeb(UART_UCR_RESET_ERROR, &uart->ucr);
	writeb(UART_UCR_RESET_MR, &uart->ucr);
	__asm__("nop");

	writeb(0, &uart->uimr);

	/* write to CSR: RX/TX baud rate from timers */
	writeb(UART_UCSR_RCS_SYS_CLK | UART_UCSR_TCS_SYS_CLK, &uart->ucsr);

	writeb(UART_UMR_BC_8 | UART_UMR_PM_NONE, &uart->umr1);
	writeb(UART_UMR_SB_STOP_BITS_1, &uart->umr2);

	/* Setting up BaudRate, using u-boot correction  */
	counter = (u32) ((gd.bus_clk / 32) + (baudrate / 2));
	counter = counter / baudrate;

	/* write to CTUR: divide counter upper byte */
	writeb((u8)((counter & 0xff00) >> 8), &uart->ubg1);
	/* write to CTLR: divide counter lower byte */
	writeb((u8)(counter & 0x00ff), &uart->ubg2);

	writeb(UART_UCR_RX_ENABLED | UART_UCR_TX_ENABLED, &uart->ucr);
}

void serial_putc(unsigned char c)
{
	struct uart_regs *uart = (struct uart_regs *)CONFIG_SYS_UART_BASE;

	if (c == '\n')
		serial_putc('\r');

	/* Wait for last character to go. */
	while (!(readb(&uart->usr) & UART_USR_TXRDY))
		;

	writeb(c, &uart->utb);
}

int serial_getchar(int timeout)
{
	struct uart_regs *uart = (struct uart_regs *)CONFIG_SYS_UART_BASE;
	int ticks = 0;

	/* Wait for a character to arrive. */

	if (timeout) {
		while (!(readb(&uart->usr) & UART_USR_RXRDY)) {
			if (ticks++ > GETCHAR_TIMEOUT_TICKS_MAX)
				return GETCHAR_TIMEOUT;
		}
		return readb(&uart->urb);
	} else {
		while (!(readb(&uart->usr) & UART_USR_RXRDY))
			;
	}

	return readb(&uart->urb);
}

void serial_write(u8 *buff)
{
	int size = c_str_length(buff);

	while(size--)
		serial_putc(*buff++);
}

