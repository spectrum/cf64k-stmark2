/*
 * spi_flash.c
 *
 * mcf54416 sram bootloader spi flash driver
 *
 * Copyright 2016 Angelo Dureghello - Sysam <angelo@sysam.it>
 *
 * This file is part of cf64k firmware application.
 *
 * cf64k is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cf64k.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Note: most of the code and related defines comes from
 *       the U-boot bootloader (www.denx.de).
 */

/*
 * converting this driver to accept qspi types
 */

#include "spi.h"
#include "spi_flash.h"

#define SPI_FLASH_PORT		0
#define SPI_FLASH_CS		1

#define FLASH_CMD_READ_JEDEC_ID	0x9f
#define FLASH_CMD_WRITE_STATUS	0x01
#define FLASH_CMD_PAGE_PROGR	0x02
#define FLASH_CMD_READ		0x03
#define FLASH_CMD_WRITE_DIS	0x04
#define FLASH_CMD_READ_STATUS	0x05
#define FLASH_CMD_WRITE_EN	0x06
#define FLASH_CMD_SECT_ERASE	0x20
#define FLASH_CMD_EWSR		0x50
#define FLASH_CMD_BLOCK_ERASE	0x52
#define FLASH_CMD_CHIP_ERASE	0x60

#define FLASH_ST_BUSY		(1 << 0)
#define FLASH_ST_WEL		(1 << 1)
#define FLASH_ST_BP0		(1 << 2)
#define FLASH_ST_BP1		(1 << 3)
#define FLASH_ST_AAI		(1 << 6)

#define REG_STATUS2_QE		(1 << 1)

/* QSPI */
#define FLASH_CMD_READ_ST2	0x35
#define FLASH_CMD_WRITE_ST2	0x31

struct jedec_id jid = {0, 0, 0};

static inline void spi_setup_addr(u32 address, u8 *addr)
{
	addr[0] = (address >> 16) & 0xff;
	addr[1] = (address >> 8) & 0xff;
	addr[2] = address & 0xff;
}

static u8 spi_read_status_register(void)
{
	u8 txbuff = FLASH_CMD_READ_STATUS;
	u8 status = 0;

	dspi_write_buff(SPI_FLASH_PORT, SPI_FLASH_CS, &txbuff,
			sizeof(txbuff), 0);
	dspi_read_buff(SPI_FLASH_PORT, SPI_FLASH_CS, &status, sizeof(status));

	return status;
}

static void spi_enable_write_status_register(void)
{
	u8 txbuff = FLASH_CMD_EWSR;

	dspi_write_buff(SPI_FLASH_PORT, SPI_FLASH_CS, &txbuff,
			sizeof(txbuff), 1);
}

static void spi_write_status_register(u8 value)
{
	u8 txbuff[2] = { FLASH_CMD_WRITE_STATUS, 0 };

	txbuff[1] = value;

	dspi_write_buff(SPI_FLASH_PORT, SPI_FLASH_CS, txbuff,
			sizeof(txbuff), 1);
}

static void spi_remove_block_protection(void)
{
	u8 status = spi_read_status_register();

	status &= ~(FLASH_ST_BP0 | FLASH_ST_BP1);

	spi_enable_write_status_register();
	spi_write_status_register(status);
}

static inline void spi_wait_completed(void)
{
	u8 status;

	do {
		status = spi_read_status_register();
	} while (status & FLASH_ST_BUSY);
}

static inline void spi_wait_wen(void)
{
	u8 status;

	do {
		status = spi_read_status_register();
	} while (!(status & FLASH_ST_WEL));
}

static void spi_flash_write_enable(void)
{
	u8 txbuff = FLASH_CMD_WRITE_EN;

	dspi_write_buff(SPI_FLASH_PORT, SPI_FLASH_CS, &txbuff,
			sizeof(txbuff), 1);

	spi_wait_wen();
}

static void spi_flash_write_disable(void)
{
	u8 txbuff = FLASH_CMD_WRITE_DIS;

	dspi_write_buff(SPI_FLASH_PORT, SPI_FLASH_CS, &txbuff,
			sizeof(txbuff), 1);
}

static int spi_read_jedec_id(void)
{
	u8 txbuff = FLASH_CMD_READ_JEDEC_ID;

	dspi_write_buff(SPI_FLASH_PORT, SPI_FLASH_CS, &txbuff,
			sizeof(txbuff), 0);
	dspi_read_buff(SPI_FLASH_PORT, SPI_FLASH_CS,  (u8 *)&jid,
		       sizeof(struct jedec_id));

	return 0;
}

void spi_flash_read_buff(u32 address, u8 *buff, u32 len)
{
	u8 txbuff[4];

	txbuff[0] = FLASH_CMD_READ;

	spi_setup_addr(address, &txbuff[1]);

	dspi_write_buff(SPI_FLASH_PORT, SPI_FLASH_CS, txbuff,
			sizeof(txbuff), 0);
	dspi_read_buff(SPI_FLASH_PORT, SPI_FLASH_CS, buff, len);
}

/*
 * Flash just check from A12 to determine sector
 */
void spi_flash_erase_sector(u32 address)
{
	u8 txbuff[4];

	spi_remove_block_protection();
	spi_flash_write_enable();

	txbuff[0] = FLASH_CMD_SECT_ERASE;

	spi_setup_addr(address, &txbuff[1]);

	dspi_write_buff(SPI_FLASH_PORT, SPI_FLASH_CS, txbuff,
			sizeof(txbuff), 1);

	spi_wait_completed();
}

void spi_flash_erase_block(u32 address)
{
	u8 txbuff[4];

	spi_remove_block_protection();
	spi_flash_write_enable();

	txbuff[0] = FLASH_CMD_BLOCK_ERASE;

	spi_setup_addr(address, &txbuff[1]);

	dspi_write_buff(SPI_FLASH_PORT, SPI_FLASH_CS, txbuff,
			sizeof(txbuff), 1);

	spi_wait_completed();
}

void spi_flash_erase_all(void)
{
	u8 txbuff = FLASH_CMD_CHIP_ERASE;

	spi_remove_block_protection();
	spi_flash_write_enable();

	dspi_write_buff(SPI_FLASH_PORT, SPI_FLASH_CS, &txbuff,
			sizeof(txbuff), 1);

	spi_wait_completed();
}

void spi_flash_byte_program(u32 address, u8 byte)
{
	u8 txbuff[5] = { FLASH_CMD_PAGE_PROGR };

	spi_setup_addr(address, &txbuff[1]);

	txbuff[4] = byte;

	dspi_write_buff(SPI_FLASH_PORT, SPI_FLASH_CS, txbuff,
			sizeof(txbuff), 1);

	spi_wait_completed();
}

void spi_flash_page_program(u32 address, u8 *buff, u32 len)
{
	u8 txbuff[5] = { FLASH_CMD_PAGE_PROGR };
	u8 status;

	if (len > 256)
		len = 256;

	spi_remove_block_protection();
	spi_flash_write_enable();

	spi_setup_addr(address, &txbuff[1]);

	txbuff[4] = *buff++;

	dspi_write_buff(SPI_FLASH_PORT, SPI_FLASH_CS, txbuff,
			sizeof(txbuff), 0);

	dspi_write_buff(SPI_FLASH_PORT, SPI_FLASH_CS, buff, len - 1, 1);
	spi_wait_completed();

	spi_flash_write_disable();

	do {
		status = spi_read_status_register();
	} while (status & FLASH_ST_AAI);
}

static u8 spi_flash_read_status2(void)
{
	u8 txbuff = FLASH_CMD_READ_ST2;
	u8 status2;

	dspi_write_buff(SPI_FLASH_PORT, SPI_FLASH_CS, &txbuff,
			sizeof(txbuff), 1);

	dspi_read_buff(SPI_FLASH_PORT, SPI_FLASH_CS, &status2, 1);

	return status2;
}

static void spi_flash_write_status2(u8 status2)
{
	u8 txbuff[2] = {FLASH_CMD_WRITE_ST2, 0};

	txbuff[1] = status2;

	dspi_write_buff(SPI_FLASH_PORT, SPI_FLASH_CS, txbuff,
			sizeof(txbuff), 1);
}

static void spi_flash_switch_to_spi_mode(void)
{
	u8 status2 = spi_flash_read_status2();

	if (status2 & REG_STATUS2_QE) {
		/* go to normal spi */
		status2 &= ~REG_STATUS2_QE;

		spi_flash_write_status2(status2);
	}
}

struct jedec_id *spi_flash_get_jedec_id(void)
{
	return &jid;
}

void init_spi_flash(void)
{
	init_dspi(SPI_FLASH_PORT);

	spi_read_jedec_id();
}
