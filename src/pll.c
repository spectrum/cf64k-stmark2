/*
 * pll.c
 *
 * mcf54416 sram bootloader ccm
 *
 * Copyright 2016 Angelo Dureghello - Sysam <angelo@sysam.it>
 *
 * This file is part of cf64k firmware application.
 *
 * cf64k is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cf64k is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cf64k.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "config.h"
#include "globals.h"
#include "ccm.h"
#include "pll.h"

#include <stdint.h>

/*
 * scheme
 *
 * ---> IN CLOCK (30mhz)---> mult to VCO freq (240 to 500Mhz)---+
 *                                                              |
 *    +---------------------------------------------------------+
 *    |
 *    | 5 dividers here, 2 to 32
 *    |
 *    +---> div for fsys (core freq) (7,5 to 250Mhz max)
 *    +---> dif for bus
 *    +---> div for USB
 *    +---> etc
 *
 * Crystal clock = fref   (14  to 50  Mhz allowed)
 * VCO = fvco             (240 to 500 Mhz)
 * 5 diviers here
 *   Core clock = fsys      ( 7.5 to 250Mhz max)
 *   Bus clock (flexbus) = fsys / 2 or / 4
 *   USB
 *   NAND
 *   eSDHC
 *
 * table of use cases (+) is the current
 *
 *   fref  fvco  fsys  fbus
 * ----------------------------------------
 * + 30    480   240
 *
 */

extern gd_t gd;

/*
 * if not from sram, settings are bootstrap settings or sbf settings
 */
#ifdef CONFIG_EXEC_FROM_SRAM
void pll_initial_setup(void)
{
	struct pll_regs *pll = (struct pll_regs *)MMAP_PLL;
	uint32_t cr, dr;
	uint32_t refdiv, fbkdiv, outdiv1, refdiv_divider;

	ccm_disable_limp_mode();

	/*
	 * IMPORTANT NOTES
	 *
	 * 1. REFDIV in all the equations must be intended as 0 based !!!
	 * 2. PLL_CR and PLL_DR must be set in 1 single 32bit write
	 * 3. LOLEN can be changed only in limp mode, keeping 1 as default
	 * 4. busclock must be half of cpu core clock
	 * 5. DDR controlled is disabled in limp mode, and limp is on at reset
	 */

	refdiv = 0;

	refdiv_divider = (refdiv == 0) ? 1 : 2;

	/*
	 * fvco = fref * ((fbkdiv + 1) / (2 ^ refdiv))
	 * so
	 * fbkdiv = (fvco / fref * 2 ^ REFDIV) - 1
	 */
	fbkdiv = (refdiv_divider * gd.vco_clk / gd.inp_clk) - 1;
	cr = in_be32(&pll->cr);

	/* clear fields and apply */
	cr &= ~PLL_CR_REFDIV(7);
	cr &= ~PLL_CR_FBKDIV(0x3f);
	cr |= PLL_CR_REFDIV(refdiv);
	cr |= PLL_CR_FBKDIV(fbkdiv);

	/* keep single write */
	out_be32(&pll->cr, cr);

	/*
	 * setting OUTDIV1 for fsys (cpu clock) as vco / 2 (1 + 1)
	 * setting OUTDIV2 for bus clock as cpu / 2 so vco / 4 (3 + 1)
	 */
	dr = in_be32(&pll->dr);

	outdiv1 = gd.vco_clk / CONFIG_SYS_CPU_CLOCK - 1;

	/* clear fields and apply */
	dr &= ~PLL_DR_OUTDIV1(0x1f);
	dr &= ~PLL_DR_OUTDIV2(0x1f);
	dr |= PLL_DR_OUTDIV1(outdiv1);
	dr |= PLL_DR_OUTDIV2(((outdiv1 + 1) << 1) - 1);

	/* keep single write */
	out_be32(&pll->dr, dr);

	/* Wait for the PLL to lock */
	while (!(in_be32(&pll->sr) & PLL_SR_LOCK));
}
#endif

int init_pll_clocks(void)
{
	struct pll_regs *pll = (struct pll_regs *)MMAP_PLL;

	uint32_t refdiv, temp, fbkdiv, vco, dr;

	/* set up globals, they will be used everywhere later,
	 * as also in serial_init() etc
	 */
	gd.inp_clk = CONFIG_SYS_INPUT_CLKSRC;
	gd.vco_clk = CONFIG_SYS_CPU_CLOCK * 2;
	/* internal bus clock must be cpu clock / 2, keeping flexbus the same */
	gd.bus_clk = CONFIG_SYS_CPU_CLOCK / 2;
	gd.flb_clk = gd.bus_clk;

#if defined(CONFIG_EXEC_FROM_SRAM) && defined(CONFIG_PLL_SETUP)
	pll_initial_setup();
#endif

	//ccm_set_flexbus_half_clock(ctrue);
}
