/*
 * cache.c
 *
 * mcf54416 cache init
 *
 * Copyright 2016 Angelo Dureghello - Sysam <angelo@sysam.it>
 *
 * This file is part of cf64k firmware application.
 *
 * cf64k is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cf64k.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Note: most of the code and related defines comes from
 *       the U-boot bootloader (www.denx.de).
 */

#include "config.h"
#include "cache.h"

static unsigned int cf_dcache_status = 0;
static unsigned int cf_icache_status = 0;

static void dcache_invalid(void)
{
#ifdef CONFIG_CF_V4
	u32 temp;

	temp = CONFIG_SYS_DCACHE_INV;
	if (*cf_dcache_status)
		temp |= CONFIG_SYS_CACHE_DCACR;
	if (*cf_icache_status)
		temp |= CONFIG_SYS_CACHE_ICACR;

	__asm__ __volatile__("movec %0, %%cacr"::"r"(temp));
#endif
}

/*
 * data cache only for ColdFire V4 such as MCF547x_8x, MCF5445x
 * the dcache will be dummy in ColdFire V2 and V3
 */
void dcache_enable(void)
{
	dcache_invalid();
	cf_dcache_status = 1;

#ifdef CONFIG_CF_V4
	__asm__ __volatile__("movec %0, %%acr0"::"r"(CONFIG_SYS_CACHE_ACR0));
	__asm__ __volatile__("movec %0, %%acr1"::"r"(CONFIG_SYS_CACHE_ACR1));
#elif defined(CONFIG_CF_V4E)
	__asm__ __volatile__("movec %0, %%acr4"::"r"(CONFIG_SYS_CACHE_ACR4));
	__asm__ __volatile__("movec %0, %%acr5"::"r"(CONFIG_SYS_CACHE_ACR5));
#endif

	__asm__ __volatile__("movec %0, %%cacr"::"r"(CONFIG_SYS_CACHE_DCACR));
}

void dcache_disable(void)
{
	u32 temp = 0;

	cf_dcache_status = 0;
	dcache_invalid();

	__asm__ __volatile__("movec %0, %%cacr"::"r"(temp));

#ifdef CONFIG_CF_V4
	__asm__ __volatile__("movec %0, %%acr0"::"r"(temp));
	__asm__ __volatile__("movec %0, %%acr1"::"r"(temp));
#elif defined(CONFIG_CF_V4E)
	__asm__ __volatile__("movec %0, %%acr4"::"r"(temp));
	__asm__ __volatile__("movec %0, %%acr5"::"r"(temp));
#endif
}
