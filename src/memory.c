/*
 * memory.c
 *
 * mcf54416 ddr init
 *
 * Copyright 2016 Angelo Dureghello - Sysam <angelo@sysam.it>
 *
 * This file is part of cf64k firmware application.
 *
 * cf64k is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cf64k.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Note: most of the code and related defines comes from
 *       the U-boot bootloader (www.denx.de).
 */

#include "trace.h"
#include "serial.h"
#include "utils.h"
#include "cache.h"

void error(unsigned int i, unsigned long *t)
{
	log("\r\nerror :( i = ");
	log(itoa(i));
	log("\r\n *t = ");
	log(itoa(*(t - 1)));
	log("\r\n\r\n");
}

void mem_test(void)
{
	unsigned int i, q;
	volatile unsigned long *t, *offs;

	clear_screen();

	log("ddr2 test step 1\r\n");
	log("each dot are 2 MB\r\n");

	for (q = 0; q < 64; ++q) {
		offs = (unsigned long *)0x40000000 + q * (1024 * 1024 * 2);
		t = offs;
		for (i = 0; i < (1024 * 512); i++) {
			*t++ = i;
		}
		t = offs;
		for (i = 0; i < (1024 * 512); i++) {
			if (*t++ != i) {
				error(i, t);
				goto exit;
			}
		}
		log(".");
	}
	log("\nddr2 test step 1 passed !\n");
	log("ddr2 test step 2, simple read\n");

	/* re read all */

	for (q = 0; q < 64; ++q) {
		offs = (unsigned long *)0x40000000 + q * (1024 * 1024 * 2);
		t = offs;
		for (i = 0; i < (1024 * 512); i++) {
			if (*t++ != i) {
				error(i, t);
				goto exit;
			}
		}
		log(".");
	}
	log("\nddr2 test step 2, simple read ok\n");
	log("ddr2 test step 3. dcache on read\n");

	dcache_disable();
	dcache_enable();

	/* re read all */

	for (q = 0; q < 64; ++q) {
		offs = (unsigned long *)0x40000000 + q * (1024 * 1024 * 2);
		t = offs;
		for (i = 0; i < (1024 * 512); i++) {
			if (*t++ != i) {
				error(i, t);
				goto exit;
			}
		}
		log(".");
	}

	log("\nddr2 test step 3 passed \\o/\n");
exit:
	log("press a key\n");

	serial_getchar(0);
}
