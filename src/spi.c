/*
 * spi.c
 *
 * mcf54416 sram bootloader spi driver
 *
 * Copyright 2016 Angelo Dureghello - Sysam <angelo@sysam.it>
 *
 * This file is part of cf64k firmware application.
 *
 * cf64k is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cf64k.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Note: most of the code and related defines comes from
 *       the U-boot bootloader (www.denx.de).
 */

#include "globals.h"
#include "gpio.h"
#include "power.h"
#include "config.h"
#include "spi.h"

static struct dspi_regs * get_module_addr(u8 spi_port)
{
	switch (spi_port) {
	default:
	case 0: return (struct dspi_regs *)MMAP_DSPI_0;
	case 1: return (struct dspi_regs *)MMAP_DSPI_1;
	case 2: return (struct dspi_regs *)MMAP_DSPI_2;
	case 3: return (struct dspi_regs *)MMAP_DSPI_3;
	}
}

static void cfspi_port_conf(u8 spi_port)
{
	struct gpio_regs *gpio = (struct gpio_regs *) MMAP_GPIO;
	struct pm_regs *pm = (struct pm_regs *) MMAP_PM;

	switch (spi_port) {
	case 0:
		out_8(&gpio->par_dspiowh,
			GPIO_PAR_DSPI0_SIN_DSPI0SIN |
			GPIO_PAR_DSPI0_SOUT_DSPI0SOUT |
			GPIO_PAR_DSPI0_SCK_DSPI0SCK);

		out_8(&gpio->par_dspiowl, 0x80);

		out_8(&gpio->srcr_dspiow, 3);

		/* DSPI0 */
		out_8(&pm->pmcr0, 23);
	break;
	}
}

void dspi_setup_slave()
{
	/*
	 * bit definition for mode:
	 * bit 31 - 28: Transfer size 3 to 16 bits
	 *     27 - 26: PCS to SCK delay prescaler
	 *     25 - 24: After SCK delay prescaler
	 *     23 - 22: Delay after transfer prescaler
	 *     21     : Allow overwrite for bit 31-22 and bit 20-8
	 *     20     : Double baud rate
	 *     19 - 16: PCS to SCK delay scaler
	 *     15 - 12: After SCK delay scaler
	 *     11 -  8: Delay after transfer scaler
	 *      7 -  0: SPI_CPHA, SPI_CPOL, SPI_LSB_FIRST
	 */
}

static u32 spi_ctrl = 0;

void dspi_write(u8 spi_port, u32 ctrl, u16 data)
{
	volatile struct dspi_regs *dspi =  get_module_addr(spi_port);

	while ((dspi->sr & 0x0000F000) >= 4)
		;

	dspi->tfr = (ctrl | data);
}

u16 dspi_read(u8 spi_port)
{
	volatile struct dspi_regs *dspi =  get_module_addr(spi_port);

	while ((dspi->sr & 0x000000F0) == 0)
		;

	return (dspi->rfr & 0xFFFF);
}

void dspi_write_buff(u8 spi_port, u8 cs, u8 *out, u16 len, char end)
{
	if (!len) return;

	spi_ctrl |= DSPI_TFR_CONT;

	/* set target */
	spi_ctrl = (spi_ctrl & 0xFF000000) | ((1 << cs) << 16);

	if (end) len--;

	/* first byte + start */
	while (len--) {
		dspi_write(spi_port, spi_ctrl, *out++);
		/* bulk read, discard */
		dspi_read(spi_port);
	}

	if (end) {
		/* end transfert */
		spi_ctrl &= ~DSPI_TFR_CONT;
		/* write remaining byte */
		dspi_write(spi_port, spi_ctrl, *out);
		/* bulk read, discard */
		dspi_read(spi_port);
	}
}

void dspi_read_buff(u8 spi_port, u8 cs, u8 *in, u16 len)
{
	if (!len) return;

	spi_ctrl |= DSPI_TFR_CONT;

	spi_ctrl = (spi_ctrl & 0xFF000000) | ((1 << cs) << 16);

	while (len > 1) {
		len--;
		/* bulk write */
		dspi_write(spi_port, spi_ctrl, CONFIG_SPI_IDLE_VAL);
		*in++ = dspi_read(spi_port);
	}

	/* end transfert */
	spi_ctrl &= ~DSPI_TFR_CONT;
	/* bulk write */
	dspi_write(spi_port, spi_ctrl, CONFIG_SPI_IDLE_VAL);
	/* remaining byte */
	*in = dspi_read(spi_port);
}

void init_dspi(u8 spi_port)
{
	volatile struct dspi_regs *dspi = get_module_addr(spi_port);

	cfspi_port_conf(spi_port);

	dspi->mcr = DSPI_MCR_MSTR |
		DSPI_MCR_CSIS1 | DSPI_MCR_CSIS0 |
		DSPI_MCR_CRXF | DSPI_MCR_CTXF;

	dspi->ctar[0] = CONFIG_SYS_DSPI_CTAR0;

#ifdef CONFIG_SYS_DSPI_CTAR1
	dspi->ctar[1] = CONFIG_SYS_DSPI_CTAR1;
#endif
#ifdef CONFIG_SYS_DSPI_CTAR2
	dspi->ctar[2] = CONFIG_SYS_DSPI_CTAR2;
#endif
}


