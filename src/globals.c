/*
 * arch.c
 *
 * mcf54416 sram bootloader arch specific inits
 *
 * Copyright 2016 Angelo Dureghello - Sysam <angelo@sysam.it>
 *
 * This file is part of cf64k firmware application.
 *
 * cf64k is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cf64k.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Note: some code and defines comes from U-boot bootloader (www.denx.de).
 *
 */

#include "globals.h"
#include "config.h"

gd_t gd;

/*
 * some minimal c support
 */
int c_str_length(u8* string)
{
	int x = 0;
	while (*string++ != 0) x++;

	return x;
}

void init_arch_globals(void)
{
	gd.bus_clk = 25000000;
}
