/*
 * main.c
 *
 * mcf54416 sram bootloader main start
 *
 * Copyright 2016 Angelo Dureghello - Sysam <angelo@sysam.it>
 *
 * This file is part of cf64k firmware application.
 *
 * cf64k is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cf64k is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cf64k.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "init.h"
#include "serial.h"
#include "spi_flash.h"
#include "trace.h"
#include "utils.h"
#include "version.h"

#include "memory.h"

#include <stdint.h>

#define sectsize (1024 * 4)

extern void ddr_init(void);

static void dump_spi(void)
{
	u8 *buff = (u8*)0x8000a000;

	clear_screen();

	log("4KB sector dump ...\r\n\r\n");

	memset(buff, 0, sectsize);
	spi_flash_read_buff(0, buff, sectsize);

	log("spi read ok!\r\n\r\n");

	log_hex_buff(buff, sectsize);

	serial_getchar(0);
}

int get_binary(void)
{
	volatile uint8_t *ptrr = (volatile uint8_t *)0x40000000;
	int i = 0, kb = 0, c;

	log("\033[2J\033[HBinary upload\r\n\r\n");
	log("Waiting ... \r\n");
	log("\x1b[s");

        while (i == 0) {
                while ((c = serial_getchar(1)) != GETCHAR_TIMEOUT) {
                        ptrr[i++] = c;
                        /* no write of info here ! or chars are lost */
                }
        }

        log("\r\n\r\nfile uploaded successfully\r\n");
	log("received ");
        logi(i);
        log(" bytes\r\n\r\n");
	log("write to spi flash (y/n) ? ");

        char x = serial_getchar(0);

        if (x == 'y')
		return i;

        log("\r\naborted\r\n");

        serial_getchar(0);

        return 0;
}

void write_binary_to_spi(int size)
{
	int sectors4k, pages, remainder;
	uint32_t spi_addr = 0;
	uint8_t *ptrr = (uint8_t *)0x40000000;

	sectors4k = size / FLASH_SECT_SIZE;
	if (size % FLASH_SECT_SIZE)
		sectors4k++;

	log("\033[2J\033[HErasing ");
	logi(sectors4k);
	log(" 4k sectors ...\r\n\r\n");

	while (sectors4k--) {
		spi_flash_erase_sector(spi_addr);
		spi_addr += FLASH_SECT_SIZE;
		log(".");
	}
	log("\n\n");

	log("\033[2J\033[HWriting ...\n");

	pages = size / FLASH_MAX_PAGE_SIZE;
	remainder = size % FLASH_MAX_PAGE_SIZE;

	spi_addr = 0;

	while (pages--) {
		spi_flash_page_program(spi_addr, ptrr, FLASH_MAX_PAGE_SIZE);
		ptrr += FLASH_MAX_PAGE_SIZE;
		spi_addr += FLASH_MAX_PAGE_SIZE;
	}

	if (remainder)
		spi_flash_page_program(spi_addr, ptrr, remainder);

	log("\r\nfile written\r\n");

	serial_getchar(0);
}

#ifdef TEST_RAW_PERIPH
static inline test_periph(void)
{
	int ch;

#define PDDR_A	0xEC09400C
#define PDDR_F	0xEC094011
#define PDDR_G	0xEC094012

#define PODR_A	0xEC094000
#define PODR_F	0xEC094005
#define PODR_G	0xEC094006

#define PPMHR0	0xFC040030
#define PPMLR0	0xFC040034
#define PPMHR1	0xFC040038
#define PPMLR1	0xFC04003C

#define PAR_FBCTL	0xEC094048
#define PAR_BE		0xEC094049
#define PAR_CS		0xEC09404A
#define PAR_IRQ0H	0xEC09404C
#define PAR_IRQ0L	0xEC09404D
#define PAR_DSPIOWH	0xEC09404E
#define PAR_DSPIOWL	0xEC09404F
#define PAR_TIMER	0xEC094050
#define PAR_UART2	0xEC094051
#define PAR_UART1	0xEC094052
#define PAR_UART0	0xEC094053
#define PAR_SDHCH	0xEC094054
#define PAR_SDHCL	0xEC094055
#define	PAR_SIMP0H	0xEC094056
#define	PAR_SIMP0L	0xEC094057
#define	PAR_SSI0H	0xEC094058
#define	PAR_SSI0L	0xEC094059
#define	PAR_DEBUGH1	0xEC09405A
#define	PAR_DEBUGH0	0xEC09405B
#define	PAR_DEBUGL	0xEC09405C
#define PAR_FEC		0xEC09405E

#define write_reg_8(x, y)  *(volatile unsigned char *)x = y;
#define write_reg_32(x, y)  *(volatile unsigned long *)x = y;


	/* pll init */
	system_init();

	write_reg_32(PPMHR0, 0);
	write_reg_32(PPMLR0, 0);
	write_reg_32(PPMHR1, 0);
	write_reg_32(PPMLR1, 0);

	write_reg_8(PAR_FBCTL, 0);
	write_reg_8(PAR_BE, 0);
	write_reg_8(PAR_CS, 0);
	write_reg_8(PAR_IRQ0H, 0);
	write_reg_8(PAR_IRQ0L, 0);
	write_reg_8(PAR_DSPIOWH, 0);
	write_reg_8(PAR_DSPIOWL, 0);
	write_reg_8(PAR_TIMER, 0);
	write_reg_8(PAR_UART2, 0);
	write_reg_8(PAR_UART1, 0);
	write_reg_8(PAR_UART0, 0);
	write_reg_8(PAR_SDHCL, 0);
	write_reg_8(PAR_SDHCH, 0);
	write_reg_8(PAR_SSI0H, 0);
	write_reg_8(PAR_SIMP0H, 0);
	write_reg_8(PAR_SIMP0L, 0);
	write_reg_8(PAR_SSI0H, 0);
	write_reg_8(PAR_SSI0L, 0);
	write_reg_8(PAR_DEBUGH1, 0);
	write_reg_8(PAR_DEBUGH0, 0);
	write_reg_8(PAR_DEBUGL, 0);
	write_reg_8(PAR_FEC, 0);

	/*
	 * testing a square wave on PORTA, UART0_TX, UART1_TX, and all SDCH pins
	 *
	 * PF0   SDHC_DAT1
	 * PF1   SDHC_DAT2
	 * PF2   SDHC_DAT3
	 * PF3   UART0_TX
	 * PF7   URAT1_TX
	 *
	 * PG5   SDHC_CLK
	 * PG6   SDHC_CMD
	 * PG7   SDHC_DAT0
	 *
	 */
	write_reg_8(PDDR_A, 0xff);
	write_reg_8(PDDR_F, 0x8f);
	write_reg_8(PDDR_G, 0xff);

	for(;;) {
		write_reg_8(PODR_A, 0x00);
		write_reg_8(PODR_F, 0x00);
		write_reg_8(PODR_G, 0x00);
		for (ch = 0; ch < 1000; ++ch);
		write_reg_8(PODR_A, 0xff);
		write_reg_8(PODR_F, 0xff);
		write_reg_8(PODR_G, 0xff);
		for (ch = 0; ch < 1000; ++ch);
	}
}
#endif

int __main(void)
{
	int i, ch;
#ifdef TEST_RAW_PERIPH
	test_periph();
#endif
	system_init();

	ddr_init();

	for (;;) {
		clear_screen();
		log(CL_YELL "Greetings !\n");
		log(CL_CYAN "cf64 bootloader v. "
			CL_GREE version CL_WHIT "\r\n\r\n");
		log("spi jedec id: ");
		log_hex_buff((u8 *)spi_flash_get_jedec_id(),
			     sizeof(struct jedec_id));
		log("\n\n");

		log(CL_YELL "1..." CL_GREE " dump spi 4kb sector\n");
		log(CL_YELL "2..." CL_MAG " write binary to spi, part. u-boot\n");
		log(CL_YELL "6..." CL_CYAN " ddr2 memtest\n\n");
		log(CL_YELL "please select ...\n\n");

		ch = serial_getchar(0);
		ch -= 48;

		switch (ch) {
		case 1:
			dump_spi();
			break;
		case 2:
			if (i = get_binary())
				write_binary_to_spi(i);
			break;
		case 3:
		case 4:
			break;
		case 6:
			mem_test();
			break;
		}
	}

	return 0;
}
