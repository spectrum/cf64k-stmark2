#ifndef __pll_h
#define __pll_h

#include "arch.h"

#define PLL_CR_LOLEN		(1 << 12)
#define PLL_CR_REFDIV_BITS	0x03
#define PLL_CR_REFDIV_POS	8
#define PLL_CR_FBKDIV_BITS	0x3f
#define PLL_DR_OUTDIV2_POS	5
#define PLL_DR_OUTDIV3_POS	10
#define PLL_DR_OUTDIV4_POS	16
#define PLL_DR_OUTDIV5_POS	21
#define PLL_DR_OUTDIV_BITS	0x1f

#define PLL_DR_OUTDIV1(x)	(x & PLL_DR_OUTDIV_BITS)
#define PLL_DR_OUTDIV2(x)	((x & PLL_DR_OUTDIV_BITS) \
				<< PLL_DR_OUTDIV2_POS)
#define PLL_DR_OUTDIV3(x)	((x & PLL_DR_OUTDIV_BITS) \
				<< PLL_DR_OUTDIV3_POS)
#define PLL_DR_OUTDIV4(x)	((x & PLL_DR_OUTDIV_BITS) \
				<< PLL_DR_OUTDIV4_POS)
#define PLL_DR_OUTDIV5(x)	((x & PLL_DR_OUTDIV_BITS) \
				<< PLL_DR_OUTDIV5_POS)

#define PLL_CR_REFDIV(x)	((x & PLL_CR_REFDIV_BITS) << PLL_CR_REFDIV_POS)
#define PLL_CR_FBKDIV(x)	(x & PLL_CR_FBKDIV_BITS)

#define PLL_SR_LOCK		(1 << 4)

/* Phase Locked Loop (PLL) */
struct pll_regs {
	u32 cr;	/* Control */
	u32 dr;	/* Divider */
	u32 sr;	/* Status */
};

int init_pll_clocks(void);

#endif
