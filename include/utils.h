#ifndef __utils_h
#define __utils_h

int strlen(char *str);
char * itoa(int val);
char * itohex(int val, unsigned char caps);
char * zpad(char *sz, int times);
void memset(char *off, char c, int len);

#endif // __utils_h
