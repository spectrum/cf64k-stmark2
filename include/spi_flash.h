#ifndef __spi_flash_h
#define __spi_flash_h

#define FLASH_SECT_SIZE		(1024 * 4)
#define FLASH_MAX_PAGE_SIZE	256

struct jedec_id {
	u8 manuf_id;
	u8 type;
	u8 capacity;
};

void init_spi_flash();
void spi_flash_read_byte(u32 address, u8 *byte);
void spi_flash_read_buff(u32 address, u8 *buff, u32 len);
void spi_flash_erase_sector(u32 address);
void spi_flash_erase_block(u32 address);
void spi_flash_erase_all(void);
void spi_flash_byte_program(u32 address, u8 byte);
void spi_flash_page_program(u32 address, u8 *buff, u32 len);
void spi_flash_write_buff(u32 address, u8 *buff, u32 len);

struct jedec_id *spi_flash_get_jedec_id(void);

#endif /* __spi_flash_h */
