#ifndef __scm_h
#define __scm_h

struct scm_regs {
	u8 rsvd1[19];	/* 0x00 - 0x12 */
	u8 wcr;		/* 0x13 */
	u16 rsvd2;	/* 0x14 - 0x15 */
	u16 cwcr;	/* 0x16 */
	u8 rsvd3[3];	/* 0x18 - 0x1A */
	u8 cwsr;	/* 0x1B */
	u8 rsvd4[3];	/* 0x1C - 0x1E */
	u8 scmisr;	/* 0x1F */
	u32 rsvd5;	/* 0x20 - 0x23 */
	u32 bcr;	/* 0x24 */
	u8 rsvd6[72];	/* 0x28 - 0x6F */
	u32 cfadr;	/* 0x70 */
	u8 rsvd7;	/* 0x74 */
	u8 cfier;	/* 0x75 */
	u8 cfloc;	/* 0x76 */
	u8 cfatr;	/* 0x77 */
	u32 rsvd8;	/* 0x78 - 0x7B */
	u32 cfdtr;	/* 0x7C */
};

#endif // __scm_h
