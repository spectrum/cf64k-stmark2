#ifndef __power_h
#define __power_h

struct pm_regs {
	u8 pmsr0;		/* */
	u8 pmcr0;
	u8 pmsr1;
	u8 pmcr1;
	u32 pmhr0;
	u32 pmlr0;
	u32 pmhr1;
	u32 pmlr1;
};

#endif
