#ifndef __arch_h
#define __arch_h

/* CF arch specific */
#define u8 unsigned char
#define u16 unsigned short
#define u32 unsigned long

/* other convenient types */
#define f_bool unsigned char
#define ctrue 1
#define cfalse 0

#define MMAP_PLL		0xfc0c0000
#define MMAP_UART0		0xfc060000
#define MMAP_RCM		0xec090000
#define MMAP_CCM		0xec090004
#define MMAP_GPIO		0xec094000
#define MMAP_SCM		0xfc040000
#define MMAP_PM			0xfc04002C
#define MMAP_DSPI_0		0xfc05c000
#define MMAP_DSPI_1		0xfc03c000
#define MMAP_DSPI_2		0xec038000
#define MMAP_DSPI_3		0xec03c000

#define CF_CACR_DEC		(1 << 31)
#define CF_CACR_DW		(1 << 30)
#define CF_CACR_DESB		(1 << 29)
#define CF_CACR_DDPI		(1 << 28)
#define CF_CACR_DHLCK		(1 << 27)
#define CF_CACR_DDCM_UNMASK	(0xF9FFFFFF)
#define CF_CACR_DDCM_WT		(0 << 25)
#define CF_CACR_DDCM_CB		(1 << 25)
#define CF_CACR_DDCM_P		(2 << 25)
#define CF_CACR_DDCM_IP		(3 << 25)
#define CF_CACR_DCINVA		(1 << 24)

#define CF_CACR_DDSP		(1 << 23)
#define CF_CACR_BEC		(1 << 19)
#define CF_CACR_BCINVA		(1 << 18)
#define CF_CACR_IEC		(1 << 15)
#define CF_CACR_DNFB		(1 << 13)
#define CF_CACR_IDPI		(1 << 12)
#define CF_CACR_IHLCK		(1 << 11)
#define CF_CACR_IDCM		(1 << 10)
#define CF_CACR_ICINVA		(1 << 8)
#define CF_CACR_IDSP		(1 << 7)
#define CF_CACR_EUSP		(1 << 5)
#define CF_CACR_IVO		(1 << 20)
#define CF_CACR_SPA		(1 << 14)


#define INTERNAL_SRAM_OFFSET 	0x80000000
#define INTERNAL_SRAM_SIZE	0x10000
#define ICACHE_STATUS           (INTERNAL_SRAM_OFFSET + \
				INTERNAL_SRAM_SIZE - 8)
#define DCACHE_STATUS           (INTERNAL_SRAM_OFFSET + \
				INTERNAL_SRAM_SIZE - 4)

#define CONFIG_SYS_ICACHE_INV   (CF_CACR_BCINVA + CF_CACR_ICINVA)
#define CONFIG_SYS_DCACHE_INV   (CF_CACR_DCINVA)

#define PAR_FBCTL		0xec094048
#define PAR_BE			0xec094049
#define PAR_UART0		0xec094053
#define PAR_UART1		0xec094052
#define PAR_UART2		0xec094051

#define PDDR_A			0xEC09400C
#define PDDR_B			0xEC09400D
#define PDDR_C			0xEC09400E
#define PDDR_D			0xEC09400F
#define PDDR_E			0xEC094010
#define PDDR_F			0xEC094011
#define PDDR_G			0xEC094012
#define PDDR_H			0xEC094013
#define PDDR_I			0xEC094014
#define PDDR_J			0xEC094015
#define PDDR_K			0xEC094016

#define PODR_A			0xEC094000
#define PODR_B			0xEC094001
#define PODR_C			0xEC094002
#define PODR_D			0xEC094003
#define PODR_E			0xEC094004
#define PODR_F			0xEC094005
#define PODR_G			0xEC094006
#define PODR_H			0xEC094007
#define PODR_I			0xEC094008
#define PODR_J			0xEC094009
#define PODR_K			0xEC09400A


#endif // __arch_h
