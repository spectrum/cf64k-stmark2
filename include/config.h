/*
 * this file uses U-boot (C) bootloader (www.denx.de) config style
 */
#ifndef __config_h
#define __config_h

#include "arch.h"

#define CONFIG_CF_V4E
#define CONFIG_SYS_MONITOR_BASE		0x80000000

/* the oscillator clock */
#define CONFIG_SYS_INPUT_CLKSRC		30000000
/* the clock core we want, 7,5 to 250 */
#define CONFIG_SYS_CPU_CLOCK		240000000

#define CONFIG_SYS_UART_PORT		(0)
#define CONFIG_SYS_UART_BASE		(MMAP_UART0 + \
					(CONFIG_SYS_UART_PORT * 0x4000))

#define CONFIG_BAUDRATE			115200

#define CONFIG_EXEC_FROM_SRAM
#define CONFIG_PLL_SETUP
#define CONFIG_DDR

#define CONFIG_SYS_CACHE_ICACR		(CF_CACR_BEC | CF_CACR_IEC | \
					 CF_CACR_ICINVA | CF_CACR_EUSP)
#define CONFIG_SYS_CACHE_DCACR		((CONFIG_SYS_CACHE_ICACR | \
					 CF_CACR_DEC | CF_CACR_DDCM_P | \
					 CF_CACR_DCINVA) & ~CF_CACR_ICINVA)
#define CONFIG_SYS_ICACHE_INV		(CF_CACR_BCINVA + CF_CACR_ICINVA)
#define CONFIG_SYS_DCACHE_INV		(CF_CACR_DCINVA)

#define CONFIG_SYS_CACHE_ACR2		(0x40000000 | \
					 CF_ADDRMASK(128) | \
					 CF_ACR_EN | CF_ACR_SM_ALL)

#define CONFIG_SYS_CACHE_ACR4		(0x40000000 | \
					 CF_ADDRMASK(128) | \
					 CF_ACR_EN | CF_ACR_SM_ALL)
#define CONFIG_SYS_CACHE_ACR5		0

/* #define CONFIG_SQUARE_WAVE */

#endif // __config_h


