#ifndef __cache_h
#define __cache_h

#if defined(CONFIG_CF_V4) || defined(CONFIG_CF_V4E)

#define CF_ADDRMASK(x)		(((x > 0x10) ? ((x >> 4) - 1) : (x)) << 16)
#define CF_CACR_DEC		(1 << 31)
#define CF_CACR_DW		(1 << 30)
#define CF_CACR_DESB		(1 << 29)
#define CF_CACR_DDPI		(1 << 28)
#define CF_CACR_DHLCK		(1 << 27)
#define CF_CACR_DDCM_UNMASK	(0xF9FFFFFF)
#define CF_CACR_DDCM_WT		(0 << 25)
#define CF_CACR_DDCM_CB		(1 << 25)
#define CF_CACR_DDCM_P		(2 << 25)
#define CF_CACR_DDCM_IP		(3 << 25)
#define CF_CACR_DCINVA		(1 << 24)

#define CF_CACR_DDSP		(1 << 23)
#define CF_CACR_BEC		(1 << 19)
#define CF_CACR_BCINVA		(1 << 18)
#define CF_CACR_IEC		(1 << 15)
#define CF_CACR_DNFB		(1 << 13)
#define CF_CACR_IDPI		(1 << 12)
#define CF_CACR_IHLCK		(1 << 11)
#define CF_CACR_IDCM		(1 << 10)
#define CF_CACR_ICINVA		(1 << 8)
#define CF_CACR_IDSP		(1 << 7)
#define CF_CACR_EUSP		(1 << 5)

#if defined(CONFIG_MCF5445x) || defined(CONFIG_MCF5441x)
#define CF_CACR_IVO		(1 << 20)
#define CF_CACR_SPA		(1 << 14)
#else
#define CF_CACR_DF		(1 << 4)
#endif

/* ***** ACR ***** */
#define CF_ACR_ADR_UNMASK	(0x00FFFFFF)
#define CF_ACR_ADR(x)		((x & 0xFF) << 24)
#define CF_ACR_ADRMSK_UNMASK	(0xFF00FFFF)
#define CF_ACR_ADRMSK(x)	((x & 0xFF) << 16)
#define CF_ACR_EN		(1 << 15)
#define CF_ACR_SM_UNMASK	(0xFFFF9FFF)
#define CF_ACR_SM_UM		(0 << 13)
#define CF_ACR_SM_SM		(1 << 13)
#define CF_ACR_SM_ALL		(3 << 13)
#define CF_ACR_WP		(1 << 2)

#endif

void dcache_enable(void);
void dcache_disable(void);

#endif /* __cache_h */
