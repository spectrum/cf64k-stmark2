#use only bare metal or gdb will fail
GCC_EXEC_PREFIX=/opt/toolchains/m68k/gcc-10.1.0-nolibc/m68k-linux/bin/m68k-linux-

# compile time configs
CPU=54416

CC=$(GCC_EXEC_PREFIX)gcc
LD=$(GCC_EXEC_PREFIX)ld
OBJCOPY=$(GCC_EXEC_PREFIX)objcopy

BINDIR=bin
OBJDIR=obj
SRCDIR=src
INCDIR=include
BINARY=$(BINDIR)/cf64k

INC=-I$(INCDIR)

CFLAGS=-pipe -mcpu=$(CPU) -DCONFIG_MCF5441x -fno-builtin

SRCS:= $(wildcard $(SRCDIR)/*.c)
ASMS:= $(wildcard $(SRCDIR)/*.S)
OBJS:= $(patsubst %.c,%.o,$(SRCS)) $(patsubst %.S,%.o,$(ASMS))
OBJS:= $(patsubst $(SRCDIR)%,$(OBJDIR)%,$(OBJS))

all: CFLAGS+=-Os -ffreestanding -nostdlib -ffunction-sections -fdata-sections -ffixed-d7
all: $(BINARY)

debug: CFLAGS+=-O0 -g
debug: $(BINARY)

$(BINARY): $(BINARY).elf
	$(OBJCOPY) -O binary $(BINARY).elf $@

$(BINARY).elf: $(OBJS)
	$(LD) -n -Bstatic -T ram.ld -o $(BINARY).elf $(OBJS) -Map $(BINARY).map

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) $(INC) $(CONFIG) -c -o $@ $<

$(OBJDIR)/%.o: $(SRCDIR)/%.S
	$(CC) $(CFLAGS) $(INC) $(CONFIG) -c -o $@ $<

clean:
	rm obj/*.o
	rm bin/*
